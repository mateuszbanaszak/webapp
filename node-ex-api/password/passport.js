const LocalStrategy = require("passport-local").Strategy;
const mysql = require("mysql");

var mysqlConnection = mysql.createConnection({
  host: "localhost",
  port: 3306,
  user: "root",
  password: "root12345",
  database: "uniproject",
  multipleStatements: true,
});

mysqlConnection.connect((err) => {
  if (!err) console.log("Connection Succeed!");
  else console.log("Connection Failed : " + JSON.stringify(err, undefined, 2));
});

module.exports = function (passport) {
  passport.use(
    "local",
    new LocalStrategy(
      {
        // by default, local strategy uses username and password, we will override with email
        usernameField: "username",
        passwordField: "psw",
        passReqToCallback: true, // allows us to pass back the entire request to the callback
      },
      function (req, username, psw, done) {
        // callback with email and password from our form
        mysqlConnection.query(
          "SELECT * FROM `users` WHERE `username` = '" +
            username +
            "'",
          function (err, rows) {
            if (err) return done(err);
            if (!rows.length) {
              return done(
                null,
                false
              ); 
            }

            // if the user is found but the password is wrong
            if (!(rows[0].password == psw))
              return done(
                null,
                false
              ); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, rows[0]);
          }
        );
      }
    )
  );
  passport.use('local-signup', new LocalStrategy({
    usernameField : 'username',
    passwordField : 'psw',
    passwordField : 'pswrepeat',
    passReqToCallback : true // allows us to pass back the entire request to the callback
},
function(req, email, password, done) {

    
    // we are checking to see if the user trying to login already exists
    connection.query("select * from users where username = '"+username+"'",function(err,rows){
        console.log(rows);
        console.log("above row object");
        if (err)
            return done(err);
         if (rows.length) {
            return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
        } else {

            // if there is no user with that email
            // create the user
            var newUserMysql = new Object();
            
            newUserMysql.username    = username;
            newUserMysql.psw = password; // use the generateHash function in our user model
        
            var insertQuery = "INSERT INTO users ( email, password ) values ('" + email +"','"+ password +"')";
                console.log(insertQuery);
            connection.query(insertQuery,function(err,rows){
            newUserMysql.id = rows.insertId;
            
            return done(null, newUserMysql);
            });	
        }	
    });
}));
  passport.serializeUser(function (user, done) {
    done(null, user.ID);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    mysqlConnection.query("select * from users where ID = " + id, function (
      err,
      rows
    ) {
      done(err, rows[0]);
    });
  });
};
