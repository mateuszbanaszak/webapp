module.exports = {
    ensureAuthenticated: function(req, res, next) { // ensure that user is authenticated, exist in DB with given username and password
        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect('/login');
    },
    forwardAuthenticated: function(req, res, next) { // if everything is matching user is forwarded to his/her notes
        if (!req.isAuthenticated()) {
            return next();
        }
        res.redirect('/usernotes');
    }
};