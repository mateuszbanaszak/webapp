const http = require("http");
const express = require("express");
const mysql = require("mysql");
var app = express();
var path = require("path");
const passport = require("passport");
const session = require("express-session");

const { ensureAuthenticated, forwardAuthenticated} = require('./password/auth')

app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
)

require('./password/passport')(passport);
app.use(passport.initialize());
app.use(passport.session());

app.use(express.json());
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.set('views', path.join(__dirname, 'View'));
app.set('view engine', 'pug');


// SQL
var mysqlConnection = mysql.createConnection({
  host: "localhost",
  port: 3306,
  user: "root",
  password: "root12345",
  database: "uniproject",
  multipleStatements: true,
});

mysqlConnection.connect((err) => {
  if (!err) console.log("Connection Succeed!");
  else console.log("Connection Failed : " + JSON.stringify(err, undefined, 2));
});


//GET NOTES
app.get("/usernotes", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM uniproject.notes WHERE FK_user_ID = ?", [req.user.ID] ,
    (err, rows, fields) => { 
      if (!err) res.render('firstview', { title: 'Hey', rows: rows });
      else console.log(err);
    }
  );
});

//POST NOTES
app.post("/create", (req, res) => {
  console.log(req.user.ID);
  console.log(req.body)
  mysqlConnection.query("INSERT INTO notes (Title, Description, FK_user_ID) VALUES ('" + req.body.title + "','" + req.body.body + "','" + req.user.ID + "')" , function(err, result) {
    if (err) throw err
  });
  res.redirect('/usernotes');
});

//PUT NOTES
app.post("/note/update/pug/:id", (req, res) => {
  console.log(req.body);
  mysqlConnection.query("UPDATE notes SET Title = '" + req.body.title + "', Description = '" + req.body.body + "' WHERE NoteID = '" + req.params.id + "' AND FK_user_ID = '" + req.user.ID + "'" , function(err, result) {
    if (err) throw err
  });
  res.redirect('/usernotes');
});


//DELETE NOTES
app.post("/note/delete/:id", (req, res) => {
  console.log(req.body);
  mysqlConnection.query(
    "DELETE FROM uniproject.notes WHERE NoteID = '" + req.params.id + "' AND FK_user_ID = '" + req.user.ID + "'" ,
    (err, rows, fields) => {
      if (!err) res.redirect('/usernotes')
      else console.log(err);
    }
  );
});


//LOGIN FUNCTION
app.post("/login", (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/usernotes',
    failureRedirect: '/login'
  })(req, res, next)
})

app.get("/login", forwardAuthenticated, (req, res) => {
  res.sendFile(path.join(__dirname + "/" + "Logging/login.html"))
})

//LOGOUT
app.get("/logout",(req, res) => {
  req.logOut();
  res.redirect('/login');
})

//REGISTRATION
app.post("/register", (req, res) => {
var usernm = req.body.username
var password = req.body.psw
var passRepeat = req.body.pswrepeat


if (password !== passRepeat) {
  res.send('<script> alert("Passwords do not match!")</script>')}
  else {
    mysqlConnection.query("SELECT * FROM `users` WHERE `username` = '"+ usernm +"'",
    (err, rows, fields) => {
      if (usernm === rows.username) {
        res.send('<script> alert("User already is in DB")</script>')
      }

      else {
        console.log(req.body)
        mysqlConnection.query("INSERT INTO users (username, password) VALUES ('" + req.body.username + "', '" + req.body.psw + "') ", function(err, result) {
          if (err) throw err})
        res.redirect('/login')
           
      };
  })}
} 
)
app.get("/register", (req, res) => {
  res.sendFile(path.join(__dirname + "/" + "Logging/index.html"))
})

//FIRST PAGE WHILE GOING TO localhost:XXXX
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/" + "Logging/index.html"));
});

//IF USER IS AUTHENTICATED -> IS FORWARDED
app.get("/usernotes", ensureAuthenticated, (req, res) => {
  console.log("Got a GET request for Welcome Page");
  console.log("Served the Welcome Page");
});


const server = http.createServer(app);
const port = 3000;
server.listen(port);

console.debug("Server listening on port " + port);